package com.example.ejemplo01;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@ComponentScan("com.example.ejemplo01")
public class Ejemplo01Application {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo01Application.class, args);
	}
        
}
