package com.example.controller;

import com.example.service.UsuarioService;
import com.example.entity.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.UnsupportedEncodingException;
import java.util.Date;

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders ="*")
@RestController
@RequestMapping({"/usuarios"})
public class Controller {
    
    @Autowired
    UsuarioService service;
    
    @GetMapping(path = "/listar")
    public List<Usuario> listar(){
        return service.listar();
    }
    
    @PostMapping
    public Usuario agregar(@RequestBody Usuario u){
        return service.add(u);
    }
    
    @GetMapping(path = {"/{id}"})
    public Usuario listarId(@PathVariable("id") int id){
        return service.listarId(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public Usuario editar(@RequestBody Usuario u, @PathVariable("id")Integer id){
        return service.add(u);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Usuario eliminar(@PathVariable("id") int id){
        return service.delete(id);
    }
/*
    @GetMapping(path = {"/auth"})
    public String getToken() throws UnsupportedEncodingException{
        String KEY="mi_clave";
        long tiempo=System.currentTimeMillis();
        String token = Jwts
                        .builder()
                        .signWith(SignatureAlgorithm.HS256,KEY)
                        .setSubject("Benito Chacon")
                        .setIssuedAt(new Date(tiempo))
                        .setExpiration(new Date(tiempo + 600000))
                        .claim("email","cercadocarlos@gmail.com").compact();
        System.out.println("------>TOKEN: " + token);
        return token;
    }*/
    
}
