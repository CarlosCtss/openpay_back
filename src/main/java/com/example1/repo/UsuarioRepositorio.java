package com.example1.repo;

import com.example.entity.Usuario;
import java.util.List;
import org.springframework.data.repository.Repository;

public interface UsuarioRepositorio extends Repository<Usuario, Integer>{

    List<Usuario> findAll();
    Usuario findById(Integer id);
    Usuario save(Usuario u);
    Usuario delete(Usuario u);
    
}
